Initialize
===========

    :::javascript

    var c = new require('NaiveBayes').NaiveBayes()


Training the classifier
=======================

Use train('text', 'category') to train the classifier.

    :::javascript
    c.train('Nobody owns the water','good');
    c.train('the quick rabbit jumps fences','good');
    c.train('buy pharmaceuticals now','bad');
    c.train('make quick money at the online casino','bad');
    c.train('the quick brown fox jumps','good');


Classifying new articles
========================

Use classify('text','defaultCategory') to classify new text.
'defaultCategory' is returned when it cannot find the category with
significant confidence.

    :::javascript
    c.classify('quick rabbit','unknown'); >>>'good'
