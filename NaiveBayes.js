var Classifier = require('./Classifier').Classifier;
var Filter = require('./Filter').Filter;
var filter = new Filter();

function NaiveBayes() {};

NaiveBayes.prototype = new Classifier();

NaiveBayes.prototype.set_threshold = function (cat, t) {
  this.thresholds[cat] = t;
}

NaiveBayes.prototype.get_threshold = function (cat) {
  if (cat in this.thresholds) return this.thresholds[cat];
  else return 1;
}

NaiveBayes.prototype.doc_prob = function (item, cat) {
  var features = filter.clean_str(item);
  var p =1;
  for (var i=0; i<features.length; i++) {
    p *= this.weighted_prob(features[i], cat, 'f_prob');
  }
  return p;
}

NaiveBayes.prototype.prob = function (item, cat) {
  var cat_p = this.cat_count(cat)/this.total_count();
  var doc_p = this.doc_prob(item, cat);
  return cat_p*doc_p;
}

NaiveBayes.prototype.classify = function (item, default_cat) {
  var probs = {};
  var max = 0;
  var best = null;
  var categories = this.categories();
  for (i in categories) {
    var cat = categories[i];
    probs[cat] = this.prob(item, cat);
    if (probs[cat] > max) {
      max = probs[cat];
      best = cat;
    }
  }
  for (i in categories) {
    if (categories[i] == best) continue;
    if (probs[categories[i]]*this.get_threshold(best) > probs[best]) return default_cat;
  }
  return best;
}


exports.NaiveBayes = NaiveBayes;
