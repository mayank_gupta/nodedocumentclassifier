var Filter = require('./Filter').Filter;
var filter = new Filter();


function Classifier () {
  //count of feature-category combination
  this.fc = {};

  //count of documents in category 
  this.cc = {};
  this.thresholds = {};
  this.minimums = {};
}

Classifier.prototype.incr_fc = function (f, cat) {
  if (typeof(this.fc[f]) == 'undefined') {
    this.fc[f] = {};
    this.fc[f][cat] = 0;
  }
  else if (typeof(this.fc[f][cat]) == 'undefined') {
    this.fc[f][cat] = 0;
  }
  this.fc[f][cat]++;
}

Classifier.prototype.incr_cc = function (cat) {
  if (!this.cc[cat]) {
    this.cc[cat] = 0;
  }
  this.cc[cat]++;
}

Classifier.prototype.f_count = function (f, cat) {
  if (f in this.fc  &&  cat in this.fc[f]) {
    return parseFloat(this.fc[f][cat]);
  }
  return 0;
}

Classifier.prototype.cat_count = function (cat) {
  if (cat in this.cc) {
    return parseFloat(this.cc[cat]);
  }
  return 0;
}

Classifier.prototype.total_count = function () {
  var sum = 0;
  for (var i in this.cc) {
    sum += this.cc[i];
  }
  return sum;
}

Classifier.prototype.f_prob = function (f, cat) {
  if (this.cat_count(cat) == 0) {
    return 0;
  }
  return this.f_count(f, cat)/this.cat_count(cat);
}

Classifier.prototype.weighted_prob = function (f, cat, bp, weight, assumed_prob) {
  if (!weight) weight = 1.0;
  if (!assumed_prob) assumed_prob = 0.5;

  var basic_prob = (bp=='f_prob')? this.f_prob(f, cat): this.c_prob(f, cat);

  //number of times feature occured in all categories
  var category_list = this.categories();
  var total = 0;
  for (i in category_list) {
    total += this.f_count(f, category_list[i]);
  }

  var bp = ((weight*assumed_prob) + (total*basic_prob))/(weight + total);
  return bp;
}

Classifier.prototype.categories = function () {
  var category_list = [];
  for (var i in this.cc) {
    category_list.push(i);
  }
  return category_list;
}

Classifier.prototype.train = function (item, cat) {
  features = filter.clean_str(item);
  for (var i=0; i<features.length; i++) {
    this.incr_fc(features[i], cat);
  }
  this.incr_cc(cat);
}


exports.Classifier = Classifier;
