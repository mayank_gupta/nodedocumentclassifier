var _ = require('lodash');
var fs = require('fs');
var sw_dir = './stop-words';

function Filter() {

  this.language = /english/i;
  var files = fs.readdirSync(sw_dir);
  this.stop_words = _.compact([]);
  for (var i=0; i<files.length; i++) {
    if (this.language.test(files[i]) == true) {
      var stop_words_string = fs.readFileSync(sw_dir + '/' + files[i]).toString();
      var sw_arr = stop_words_string.split(/(\s|\n|\r|\r\n)/);
      this.stop_words = _.union(this.stop_words, sw_arr);
    }
  }
}

Filter.prototype.print = function () {
  console.log(this.stop_words);
}

Filter.prototype.clean = function (tokens) {
  return _.difference(tokens,this.stop_words);
}

Filter.prototype.clean_str = function (text) {
  var tokens = text.toLowerCase().replace(/[,,",;]/g,"").split(/\s+/);
  for (var i=0; i<tokens.length; i++) {
    tokens[i] = tokens[i].replace(/[\s,,]/g,"");
  }
  return this.clean(tokens);
}

exports.Filter = Filter;
