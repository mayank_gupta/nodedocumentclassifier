var Classifier = require('./Classifier').Classifier;
var Filter = require('./Filter').Filter;
var filter = new Filter();

function Fisher () {};

Fisher.prototype = new Classifier();

Fisher.prototype.c_prob = function (f, cat) {

  //frequency of feature in this category
  clf = this.f_prob(f, cat);
  if (clf == 0) return 0;

  //sum of frequencies of this feature in all categories
  var freqsum = 0;
  var categories = this.categories();
  for (i in categories) {
    freqsum += this.f_prob(f, categories[i]);
  }

  //frequency of this category divided by frequency of all categories
  return clf/freqsum;
}

Fisher.prototype.inv_chi2 = function (chi, df) {
  //Inverse chi-square because independent probabilities fit a chi-squared distribution
  
  var m = chi/2;
  var sum = Math.exp(-m);
  var term = Math.exp(-m);

  for (var i=1; i<df/2; i++) {
    term *= m/i;
    sum += term;
  }
  return sum>1 ? 1 : sum;
}

Fisher.prototype.prob = function (item, cat) {
  //Multiply all probabilities together
  var p=1;
  var features = filter.clean_str(item);

  for (i in features) {
    p *= this.weighted_prob(features[i], cat, 'c_prob');
  }

  fscore = -2*Math.log(p);
  return this.inv_chi2(fscore, features.length*2);
}

Fisher.prototype.set_minimum = function (cat, min) {
  this.minimums[cat] = min;
}

Fisher.prototype.get_minimum = function (cat) {
  return this.minimums[cat] ? this.minimums[cat] : 0;
}


Fisher.prototype.classify = function (item, default_cat) {
  var best = default_cat;
  var max = 0;

  var categories_list = this.categories();
  for (i in categories_list) {
    p = this.prob(item, categories_list[i]);
    if (p > this.get_minimum[categories_list[i]] && p>max) {
      best = categories_list[i];
      max = p;
    }
  }
  return best;
}
exports.Fisher = Fisher;

